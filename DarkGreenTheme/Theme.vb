﻿Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.ComponentModel
Imports System.Drawing.Text
Namespace Controls
#Region " Declarations & Functions "
    Enum MouseState
        None = 0
        Over = 1
        Down = 2
    End Enum
    Module Colors
        Public DarkColor As Color = Color.FromArgb(0, 75, 75)
        Public LightColor As Color = Color.FromArgb(0, 150, 150)
        Public Color1 As Color = Color.FromArgb(25, 25, 25)
        Public Color2 As Color = Color.Black
        Public Color3 As Color = Color.White
        Public TransparentColor As Color = Color.Fuchsia
    End Module
    Module Functions
        Public Function RoundRec(ByVal Rectangle As Rectangle, ByVal Curve As Integer) As GraphicsPath
            Dim P As GraphicsPath = New GraphicsPath()
            Dim ArcRectangleWidth As Integer = Curve * 2
            P.AddArc(New Rectangle(Rectangle.X, Rectangle.Y, ArcRectangleWidth, ArcRectangleWidth), -180, 90)
            P.AddArc(New Rectangle(Rectangle.Width - ArcRectangleWidth + Rectangle.X, Rectangle.Y, ArcRectangleWidth, ArcRectangleWidth), -90, 90)
            P.AddArc(New Rectangle(Rectangle.Width - ArcRectangleWidth + Rectangle.X, Rectangle.Height - ArcRectangleWidth + Rectangle.Y, ArcRectangleWidth, ArcRectangleWidth), 0, 90)
            P.AddArc(New Rectangle(Rectangle.X, Rectangle.Height - ArcRectangleWidth + Rectangle.Y, ArcRectangleWidth, ArcRectangleWidth), 90, 90)
            P.AddLine(New Point(Rectangle.X, Rectangle.Height - ArcRectangleWidth + Rectangle.Y), New Point(Rectangle.X, Curve + Rectangle.Y))
            Return P
        End Function
    End Module
#End Region
    Public Class DarkTheme : Inherits ContainerControl
#Region " Declarations "
        Private _Down As Boolean = False
        Private _Header As Integer = 30
        Private _MousePoint As Point
#End Region
#Region " Mouse States "
        Protected Overrides Sub OnMouseUp(e As MouseEventArgs)
            MyBase.OnMouseUp(e)
            _Down = False
        End Sub

        Protected Overrides Sub OnMouseDown(e As MouseEventArgs)
            MyBase.OnMouseDown(e)
            If e.Location.Y < _Header AndAlso e.Button = Windows.Forms.MouseButtons.Left Then
                _MousePoint = e.Location
                _Down = True
            End If
        End Sub

        Protected Overrides Sub OnMouseMove(e As MouseEventArgs)
            MyBase.OnMouseMove(e)
            If _Down = True Then
                ParentForm.Location = MousePosition - _MousePoint
            End If
        End Sub
#End Region
#Region " Paint "
        Protected Overrides Sub OnCreateControl()
            MyBase.OnCreateControl()
            ParentForm.FormBorderStyle = FormBorderStyle.None
            ParentForm.TransparencyKey = TransparentColor
            Dock = DockStyle.Fill
            Invalidate()
            BackColor = Color1
        End Sub

        Protected Overrides Sub OnPaint(e As PaintEventArgs)
            MyBase.OnPaint(e)
            Dim G = e.Graphics
            G.Clear(Color1)
            G.FillRectangle(New SolidBrush(Color1), New Rectangle(0, 0, Width, _Header / 2))
            G.FillRectangle(New SolidBrush(Color2), New Rectangle(0, _Header / 2, Width, _Header / 2))
            G.FillRectangle(New SolidBrush(TransparentColor), New Rectangle(0, 0, 1, 1))
            G.FillRectangle(New SolidBrush(TransparentColor), New Rectangle(1, 0, 1, 1))
            G.FillRectangle(New SolidBrush(TransparentColor), New Rectangle(0, 1, 1, 1))
            G.FillRectangle(New SolidBrush(TransparentColor), New Rectangle(Width - 1, 0, 1, 1))
            G.FillRectangle(New SolidBrush(TransparentColor), New Rectangle(Width - 2, 0, 1, 1))
            G.FillRectangle(New SolidBrush(TransparentColor), New Rectangle(Width - 1, 1, 1, 1))

            Dim _StringF As New StringFormat
            _StringF.Alignment = StringAlignment.Center
            _StringF.LineAlignment = StringAlignment.Center
            G.DrawString(Text, New Font("Segoe UI", 11, FontStyle.Bold), New SolidBrush(LightColor), New RectangleF(0, 0, Width, _Header), _StringF)
        End Sub
#End Region
    End Class
    Public Class DarkThemeControlBox : Inherits Control
#Region " Declarations "
        Private _State As MouseState = MouseState.None
        Private _MousePoint As Integer
#End Region
#Region " Mouse States "

        Protected Overrides Sub OnMouseDown(e As MouseEventArgs)
            MyBase.OnMouseDown(e)
            _State = MouseState.Down
            Invalidate()
        End Sub

        Protected Overrides Sub OnMouseUp(e As MouseEventArgs)
            MyBase.OnMouseUp(e)
            _State = MouseState.Over
            Invalidate()
        End Sub

        Protected Overrides Sub OnMouseEnter(e As EventArgs)
            MyBase.OnMouseEnter(e)
            _State = MouseState.Over
            Invalidate()
        End Sub

        Protected Overrides Sub OnMouseLeave(e As EventArgs)
            MyBase.OnMouseLeave(e)
            _State = MouseState.None
            Invalidate()
        End Sub

        Protected Overrides Sub OnMouseMove(e As MouseEventArgs)
            MyBase.OnMouseMove(e)
            _MousePoint = e.X
            Invalidate()
        End Sub

#End Region
#Region " Properties "

        Sub New()
            Size = New Size(60, 30)
            Invalidate()
        End Sub

        Protected Overrides Sub OnResize(e As EventArgs)
            MyBase.OnResize(e)
            Size = New Size(60, 30)
            Invalidate()
        End Sub

        Protected Overrides Sub OnCreateControl()
            MyBase.OnCreateControl()
            Location = New Point(Width - 60, 0)
            Invalidate()
        End Sub

        Protected Overrides Sub OnClick(e As EventArgs)
            MyBase.OnClick(e)
            If _MousePoint > 30 Then
                FindForm.Close()
            Else
                FindForm.WindowState = FormWindowState.Minimized
            End If
        End Sub
        Protected Overrides Sub OnLocationChanged(e As EventArgs)
            MyBase.OnLocationChanged(e)
            Try
                Location = New Point(Parent.Width - 60, 0)
            Catch
            End Try
        End Sub
#End Region
#Region " Paint "
        Protected Overrides Sub OnPaint(e As PaintEventArgs)
            MyBase.OnPaint(e)
            Dim G = e.Graphics
            G.Clear(Color1)
            G.FillRectangle(New SolidBrush(Color1), New Rectangle(0, 0, 60, 15))
            G.FillRectangle(New SolidBrush(Color2), New Rectangle(0, 15, 60, 15))
            Select Case _State
                Case MouseState.Over
                    If _MousePoint > 30 Then
                        G.FillRectangle(New SolidBrush(Color.FromArgb(20, Color.White)), New Rectangle(30, 0, 30, 30))
                    Else
                        G.FillRectangle(New SolidBrush(Color.FromArgb(20, Color.White)), New Rectangle(0, 0, 30, 30))
                    End If

                Case MouseState.Down
                    If _MousePoint > 30 Then
                        G.FillRectangle(New SolidBrush(Color.FromArgb(30, Color.Black)), New Rectangle(30, 0, 30, 30))
                    Else
                        G.FillRectangle(New SolidBrush(Color.FromArgb(30, Color.Black)), New Rectangle(0, 0, 30, 30))
                    End If
            End Select

            Dim _StringF As New StringFormat
            _StringF.Alignment = StringAlignment.Center
            _StringF.LineAlignment = StringAlignment.Center
            G.DrawString("r", New Font("Marlett", 11), New SolidBrush(LightColor), New RectangleF(Width - 30, 0, 30, 30), _StringF)
            G.FillRectangle(New SolidBrush(LightColor), New Rectangle(Width - 50, 14, 10, 3))
            G.FillRectangle(New SolidBrush(TransparentColor), New Rectangle(Width - 1, 0, 1, 1))
            G.FillRectangle(New SolidBrush(TransparentColor), New Rectangle(Width - 2, 0, 1, 1))
            G.FillRectangle(New SolidBrush(TransparentColor), New Rectangle(Width - 1, 1, 1, 1))
        End Sub
#End Region
    End Class
    Public Class DarkThemeButton : Inherits Control
#Region " Declarations "
        Private _State As MouseState = MouseState.None
#End Region
#Region " Mouse States "
        Protected Overrides Sub OnMouseEnter(e As EventArgs)
            MyBase.OnMouseEnter(e)
            _State = MouseState.Over
            Invalidate()
        End Sub
        Protected Overrides Sub OnMouseLeave(e As EventArgs)
            MyBase.OnMouseLeave(e)
            _State = MouseState.None
            Invalidate()
        End Sub
        Protected Overrides Sub OnMouseDown(e As MouseEventArgs)
            MyBase.OnMouseDown(e)
            _State = MouseState.Down
            Invalidate()
        End Sub
        Protected Overrides Sub OnMouseUp(e As MouseEventArgs)
            MyBase.OnMouseUp(e)
            _State = MouseState.Over
            Invalidate()
        End Sub
#End Region
#Region " Paint "
        Sub New()
            Size = New Size(100, 40)
        End Sub
        Protected Overrides Sub OnPaint(e As PaintEventArgs)
            MyBase.OnPaint(e)
            Dim G = e.Graphics
            G.Clear(DarkColor)
            G.FillRectangle(New SolidBrush(LightColor), New Rectangle(0, 0, Width, Height / 2))
            Select Case _State
                Case MouseState.Over
                    G.FillRectangle(New SolidBrush(Color.FromArgb(20, Color.White)), New Rectangle(0, 0, Width, Height))
                Case MouseState.Down
                    G.FillRectangle(New SolidBrush(Color.FromArgb(30, Color.Black)), New Rectangle(0, 0, Width, Height))
            End Select

            Dim _StringF As New StringFormat
            _StringF.Alignment = StringAlignment.Center
            _StringF.LineAlignment = StringAlignment.Center
            G.DrawString(Text, New Font("Segoe UI", 10, FontStyle.Bold), New SolidBrush(Color3), New RectangleF(0, 0, Width, Height), _StringF)
            G.FillRectangle(New SolidBrush(Parent.BackColor), New Rectangle(0, 0, 0, 0))
            G.FillRectangle(New SolidBrush(Parent.BackColor), New Rectangle(0, 0, 1, 1))
            G.FillRectangle(New SolidBrush(Parent.BackColor), New Rectangle(0, 1, 1, 1))
            G.FillRectangle(New SolidBrush(Parent.BackColor), New Rectangle(1, 0, 1, 1))
            G.FillRectangle(New SolidBrush(Parent.BackColor), New Rectangle(Width - 1, 0, 1, 1))
            G.FillRectangle(New SolidBrush(Parent.BackColor), New Rectangle(Width - 1, 1, 1, 1))
            G.FillRectangle(New SolidBrush(Parent.BackColor), New Rectangle(Width - 2, 0, 1, 1))
            G.FillRectangle(New SolidBrush(Parent.BackColor), New Rectangle(0, Height - 1, 1, 1))
            G.FillRectangle(New SolidBrush(Parent.BackColor), New Rectangle(0, Height - 2, 1, 1))
            G.FillRectangle(New SolidBrush(Parent.BackColor), New Rectangle(1, Height - 1, 1, 1))
            G.FillRectangle(New SolidBrush(Parent.BackColor), New Rectangle(Width - 1, Height - 1, 1, 1))
            G.FillRectangle(New SolidBrush(Parent.BackColor), New Rectangle(Width - 1, Height - 2, 1, 1))
            G.FillRectangle(New SolidBrush(Parent.BackColor), New Rectangle(Width - 2, Height - 1, 1, 1))
        End Sub
#End Region
    End Class
    <DefaultEvent("CheckedChanged")>
    Public Class DarkThemeRadioButton : Inherits Control
#Region " Declarations "
        Private _Checked As Boolean
        Private _State As MouseState = MouseState.None
#End Region
#Region " Mouse States "
        Protected Overrides Sub OnMouseEnter(e As EventArgs)
            MyBase.OnMouseEnter(e)
            _State = MouseState.Over
            Invalidate()
        End Sub
        Protected Overrides Sub OnMouseLeave(e As EventArgs)
            MyBase.OnMouseLeave(e)
            _State = MouseState.None
            Invalidate()
        End Sub
        Protected Overrides Sub OnMouseUp(e As MouseEventArgs)
            MyBase.OnMouseUp(e)
            _State = MouseState.Over
            Invalidate()
        End Sub
        Protected Overrides Sub OnMouseDown(e As MouseEventArgs)
            MyBase.OnMouseDown(e)
            _State = MouseState.Down
            Invalidate()
        End Sub

#End Region
#Region " Properties "
        Property Checked() As Boolean
            Get
                Return _Checked
            End Get
            Set(value As Boolean)
                _Checked = value
                InvalidateControls()
                RaiseEvent CheckedChanged(Me)
                Invalidate()
            End Set
        End Property
        Event CheckedChanged(ByVal sender As Object)
        Protected Overrides Sub OnClick(e As EventArgs)
            If Not _Checked Then Checked = True
            MyBase.OnClick(e)
        End Sub
        Private Sub InvalidateControls()
            If Not IsHandleCreated OrElse Not _Checked Then Return
            For Each C As Control In Parent.Controls
                If C IsNot Me AndAlso TypeOf C Is DarkThemeRadioButton Then
                    DirectCast(C, DarkThemeRadioButton).Checked = False
                    Invalidate()
                End If
            Next
        End Sub
        Protected Overrides Sub OnCreateControl()
            MyBase.OnCreateControl()
            InvalidateControls()
        End Sub


        Protected Overrides Sub OnResize(e As EventArgs)
            MyBase.OnResize(e)
            Height = 16
        End Sub

#End Region
#Region " Paint "
        Protected Overrides Sub OnPaint(e As PaintEventArgs)
            MyBase.OnPaint(e)
            Dim G = e.Graphics
            G.SmoothingMode = 2
            G.TextRenderingHint = 5

            G.Clear(Parent.BackColor)


            G.FillEllipse(New SolidBrush(Color3), New Rectangle(0, 0, 15, 15))

            Select Case _State
                Case MouseState.Over
                    G.FillEllipse(New SolidBrush(LightColor), New Rectangle(0, 0, 15, 15))
                Case MouseState.Down
                    G.FillEllipse(New SolidBrush(Color.FromArgb(15, Color.Black)), New Rectangle(0, 0, 15, 15))
            End Select

            If Checked Then
                G.FillEllipse(New SolidBrush(DarkColor), New Rectangle(4, 4, 7, 7))

            End If
            G.DrawString(Text, New Font("Segoe UI", 10, FontStyle.Bold), New SolidBrush(Color3), New Point(18, -2))
        End Sub
#End Region
    End Class
    <DefaultEvent("CheckedChanged")>
    Public Class DarkThemeCheckBox : Inherits Control
#Region " Variables "
        Private _State As MouseState = MouseState.None
        Private _Checked As Boolean
#End Region
#Region " Properties "
        Protected Overrides Sub OnTextChanged(ByVal e As System.EventArgs)
            MyBase.OnTextChanged(e)
            Invalidate()
        End Sub

        Property Checked() As Boolean
            Get
                Return _Checked
            End Get
            Set(ByVal value As Boolean)
                _Checked = value
                Invalidate()
            End Set
        End Property

        Event CheckedChanged(ByVal sender As Object)
        Protected Overrides Sub OnClick(ByVal e As System.EventArgs)
            _Checked = Not _Checked
            RaiseEvent CheckedChanged(Me)
            MyBase.OnClick(e)
        End Sub

        Protected Overrides Sub OnResize(e As EventArgs)
            MyBase.OnResize(e)
            Height = 16
        End Sub

#End Region
#Region " Mouse States "

        Protected Overrides Sub OnMouseDown(e As MouseEventArgs)
            MyBase.OnMouseDown(e)
            _State = MouseState.Down
            Invalidate()
        End Sub
        Protected Overrides Sub OnMouseUp(e As MouseEventArgs)
            MyBase.OnMouseUp(e)
            _State = MouseState.Over
            Invalidate()
        End Sub
        Protected Overrides Sub OnMouseEnter(e As EventArgs)
            MyBase.OnMouseEnter(e)
            _State = MouseState.Over
            Invalidate()
        End Sub
        Protected Overrides Sub OnMouseLeave(e As EventArgs)
            MyBase.OnMouseLeave(e)
            _State = MouseState.None
            Invalidate()
        End Sub

#End Region
#Region " Paint "
        Protected Overrides Sub OnPaint(e As PaintEventArgs)
            MyBase.OnPaint(e)
            Dim G = e.Graphics
            G.Clear(Parent.BackColor)

            G.FillRectangle(New SolidBrush(Color3), New Rectangle(0, 0, 15, 15))

            G.FillRectangle(New SolidBrush(Parent.BackColor), New Rectangle(0, 0, 1, 1))
            G.FillRectangle(New SolidBrush(Parent.BackColor), New Rectangle(0, 14, 1, 1))
            G.FillRectangle(New SolidBrush(Parent.BackColor), New Rectangle(14, 0, 1, 1))
            G.FillRectangle(New SolidBrush(Parent.BackColor), New Rectangle(14, 14, 1, 1))

            If Checked Then
                G.DrawString("ü", New Font("Wingdings", 9, FontStyle.Bold), New SolidBrush(DarkColor), New Point(1, 1))

            End If
            G.DrawString(Text, New Font("Segoe UI", 10, FontStyle.Bold), New SolidBrush(Color3), New Point(18, -1))
        End Sub
#End Region
    End Class
    <DefaultEvent("CheckedChanged")>
    Public Class DarkThemeToggle : Inherits Control
#Region " Properties "
        Public Event CheckedChanged(ByVal sender As Object)
        Private _Checked As Boolean
        Public Property Checked As Boolean
            Get
                Return _Checked
            End Get
            Set(value As Boolean)
                _Checked = value
                Invalidate()
            End Set
        End Property
#End Region
#Region " Paint "
        Sub New()
            MyBase.New()
            SetStyle(ControlStyles.UserPaint Or ControlStyles.ResizeRedraw Or ControlStyles.SupportsTransparentBackColor, True)
            DoubleBuffered = True
            BackColor = Color2
            Size = New Size(80, 25)
        End Sub
        Protected Overrides Sub OnResize(e As EventArgs)
            MyBase.OnResize(e)
            Size = New Size(80, 25)
        End Sub
        Protected Overrides Sub OnClick(ByVal e As EventArgs)
            MyBase.OnClick(e)
            Checked = Not Checked
            RaiseEvent CheckedChanged(Me)
        End Sub
        Protected Overrides Sub OnPaint(ByVal e As PaintEventArgs)
            Dim b As Bitmap = New Bitmap(Width, Height)
            Dim g As Graphics = Graphics.FromImage(b)
            Dim rect As Rectangle = New Rectangle(0, 0, Width - 1, Height - 1)
            Dim onrect As Rectangle = New Rectangle(CInt((Width / 2) - 2), 2, CInt((Width / 2) - 1), Height - 5)
            Dim offrect As Rectangle = New Rectangle(2, 2, CInt((Width / 2) - 1), Height - 5)
            MyBase.OnPaint(e)
            g.Clear(BackColor)
            g.SmoothingMode = SmoothingMode.HighQuality
            g.InterpolationMode = InterpolationMode.HighQualityBicubic
            g.FillRectangle(New SolidBrush(Color3), rect)
            If Checked Then
                g.FillPath(New SolidBrush(DarkColor), RoundRec(onrect, 3))
                g.DrawPath(New Pen(New SolidBrush(Color2)), RoundRec(onrect, 3))
                g.DrawString("ON", New Font("Segoe UI", 10, FontStyle.Bold), New SolidBrush(Color2), New Rectangle(0, 5, CInt((Width / 2)), 15), New StringFormat() With {.Alignment = StringAlignment.Center, .LineAlignment = StringAlignment.Center})
                g.DrawLine(New Pen(Color2), 56, 5, 56, Height - 7)
                g.DrawLine(New Pen(Color2), 58, 3, 58, Height - 5)
                g.DrawLine(New Pen(Color2), 60, 5, 60, Height - 7)
            Else
                g.FillPath(New SolidBrush(DarkColor), RoundRec(offrect, 3))
                g.DrawPath(New Pen(New SolidBrush(Color2)), RoundRec(offrect, 3))
                g.DrawString("OFF", New Font("Segoe UI", 10, FontStyle.Bold), New SolidBrush(Color2), New Rectangle(CInt((Width / 2)), 5, CInt((Width / 2)), 15), New StringFormat() With {.Alignment = StringAlignment.Center, .LineAlignment = StringAlignment.Center})
                g.DrawLine(New Pen(Color2), 20, 5, 20, Height - 7)
                g.DrawLine(New Pen(Color2), 22, 3, 22, Height - 5)
                g.DrawLine(New Pen(Color2), 24, 5, 24, Height - 7)
            End If
            e.Graphics.DrawImage(b, New Point(0, 0))
            g.Dispose() : b.Dispose()
        End Sub
#End Region
    End Class
    <DefaultEvent("TextChanged")>
    Class DarkThemeTextBox : Inherits Control
#Region " Variables "

        Private _State As MouseState = MouseState.None
        Private WithEvents TB As Windows.Forms.TextBox

#End Region
#Region " Properties "
        Private _TextAlign As HorizontalAlignment = HorizontalAlignment.Left
        <Category("Options")> _
        Property TextAlign() As HorizontalAlignment
            Get
                Return _TextAlign
            End Get
            Set(ByVal value As HorizontalAlignment)
                _TextAlign = value
                If TB IsNot Nothing Then
                    TB.TextAlign = value
                End If
            End Set
        End Property
        Private _MaxLength As Integer = 32767
        <Category("Options")> _
        Property MaxLength() As Integer
            Get
                Return _MaxLength
            End Get
            Set(ByVal value As Integer)
                _MaxLength = value
                If TB IsNot Nothing Then
                    TB.MaxLength = value
                End If
            End Set
        End Property
        Private _ReadOnly As Boolean
        <Category("Options")> _
        Property [ReadOnly]() As Boolean
            Get
                Return _ReadOnly
            End Get
            Set(ByVal value As Boolean)
                _ReadOnly = value
                If TB IsNot Nothing Then
                    TB.ReadOnly = value
                End If
            End Set
        End Property
        Private _UseSystemPasswordChar As Boolean
        <Category("Options")> _
        Property UseSystemPasswordChar() As Boolean
            Get
                Return _UseSystemPasswordChar
            End Get
            Set(ByVal value As Boolean)
                _UseSystemPasswordChar = value
                If TB IsNot Nothing Then
                    TB.UseSystemPasswordChar = value
                End If
            End Set
        End Property
        Private _Multiline As Boolean
        <Category("Options")> _
        Property Multiline() As Boolean
            Get
                Return _Multiline
            End Get
            Set(ByVal value As Boolean)
                _Multiline = value
                If TB IsNot Nothing Then
                    TB.Multiline = value

                    If value Then
                        TB.Height = Height - 11
                    Else
                        Height = TB.Height + 11
                    End If

                End If
            End Set
        End Property
        <Category("Options")> _
        Overrides Property Text As String
            Get
                Return MyBase.Text
            End Get
            Set(ByVal value As String)
                MyBase.Text = value
                If TB IsNot Nothing Then
                    TB.Text = value
                End If
            End Set
        End Property
        <Category("Options")> _
        Overrides Property Font As Font
            Get
                Return MyBase.Font
            End Get
            Set(ByVal value As Font)
                MyBase.Font = value
                If TB IsNot Nothing Then
                    TB.Font = value
                    TB.Location = New Point(3, 5)
                    TB.Width = Width - 6

                    If Not _Multiline Then
                        Height = TB.Height + 11
                    End If
                End If
            End Set
        End Property

        Protected Overrides Sub OnCreateControl()
            MyBase.OnCreateControl()
            If Not Controls.Contains(TB) Then
                Controls.Add(TB)
            End If
        End Sub
        Private Sub OnBaseTextChanged(ByVal s As Object, ByVal e As EventArgs)
            Text = TB.Text
        End Sub
        Private Sub OnBaseKeyDown(ByVal s As Object, ByVal e As KeyEventArgs)
            If e.Control AndAlso e.KeyCode = Keys.A Then
                TB.SelectAll()
                e.SuppressKeyPress = True
            End If
            If e.Control AndAlso e.KeyCode = Keys.C Then
                TB.Copy()
                e.SuppressKeyPress = True
            End If
        End Sub
        Protected Overrides Sub OnResize(ByVal e As EventArgs)
            TB.Location = New Point(5, 5)
            TB.Width = Width - 10

            If _Multiline Then
                TB.Height = Height - 11
            Else
                Height = TB.Height + 11
            End If

            MyBase.OnResize(e)
        End Sub

#End Region
#Region " Mouse States "

        Protected Overrides Sub OnMouseDown(e As MouseEventArgs)
            MyBase.OnMouseDown(e)
            _State = MouseState.Down
            Invalidate()
        End Sub
        Protected Overrides Sub OnMouseUp(e As MouseEventArgs)
            MyBase.OnMouseUp(e)
            _State = MouseState.Over
            TB.Focus()
            Invalidate()
        End Sub
        Protected Overrides Sub OnMouseEnter(e As EventArgs)
            MyBase.OnMouseEnter(e)
            _State = MouseState.Over
            TB.Focus()
            Invalidate()
        End Sub
        Protected Overrides Sub OnMouseLeave(e As EventArgs)
            MyBase.OnMouseLeave(e)
            _State = MouseState.None
            Invalidate()
        End Sub

#End Region
#Region " Paint "
        Sub New()
            SetStyle(ControlStyles.AllPaintingInWmPaint Or ControlStyles.UserPaint Or _
                     ControlStyles.ResizeRedraw Or ControlStyles.OptimizedDoubleBuffer Or _
                     ControlStyles.SupportsTransparentBackColor, True)
            DoubleBuffered = True

            BackColor = Color.Transparent

            TB = New Windows.Forms.TextBox
            TB.Font = New Font("Segoe UI", 10, FontStyle.Bold)
            TB.Text = Text
            TB.BackColor = Color3
            TB.ForeColor = LightColor
            TB.MaxLength = _MaxLength
            TB.Multiline = _Multiline
            TB.ReadOnly = _ReadOnly
            TB.UseSystemPasswordChar = _UseSystemPasswordChar
            TB.BorderStyle = BorderStyle.None
            TB.Location = New Point(5, 5)
            TB.Width = Width - 10

            TB.Cursor = Cursors.IBeam

            If _Multiline Then
                TB.Height = Height - 11
            Else
                Height = TB.Height + 11
            End If

            AddHandler TB.TextChanged, AddressOf OnBaseTextChanged
            AddHandler TB.KeyDown, AddressOf OnBaseKeyDown
        End Sub

        Protected Overrides Sub OnPaint(e As PaintEventArgs)
            MyBase.OnPaint(e)
            Dim G = e.Graphics
            G.Clear(Color3)

            G.FillRectangle(New SolidBrush(Parent.BackColor), New Rectangle(0, 0, 1, 1))
            G.FillRectangle(New SolidBrush(Parent.BackColor), New Rectangle(0, 1, 1, 1))
            G.FillRectangle(New SolidBrush(Parent.BackColor), New Rectangle(1, 0, 1, 1))
            G.FillRectangle(New SolidBrush(Parent.BackColor), New Rectangle(Width - 1, 0, 1, 1))
            G.FillRectangle(New SolidBrush(Parent.BackColor), New Rectangle(Width - 1, 1, 1, 1))
            G.FillRectangle(New SolidBrush(Parent.BackColor), New Rectangle(Width - 2, 0, 1, 1))
            G.FillRectangle(New SolidBrush(Parent.BackColor), New Rectangle(0, Height - 1, 1, 1))
            G.FillRectangle(New SolidBrush(Parent.BackColor), New Rectangle(0, Height - 2, 1, 1))
            G.FillRectangle(New SolidBrush(Parent.BackColor), New Rectangle(1, Height - 1, 1, 1))
            G.FillRectangle(New SolidBrush(Parent.BackColor), New Rectangle(Width - 1, Height - 1, 1, 1))
            G.FillRectangle(New SolidBrush(Parent.BackColor), New Rectangle(Width - 1, Height - 2, 1, 1))
            G.FillRectangle(New SolidBrush(Parent.BackColor), New Rectangle(Width - 2, Height - 1, 1, 1))

        End Sub
#End Region
    End Class
    Class DarkThemeProgressBar : Inherits Control
#Region " Variables "

        Private _Value As Integer = 0
        Private _Maximum As Integer = 100

#End Region
#Region " Control "
        <Category("Control")>
        Public Property Maximum() As Integer
            Get
                Return _Maximum
            End Get
            Set(V As Integer)
                Select Case V
                    Case Is < _Value
                        _Value = V
                End Select
                _Maximum = V
                Invalidate()
            End Set
        End Property

        <Category("Control")>
        Public Property Value() As Integer
            Get
                Select Case _Value
                    Case 0
                        Return 0
                        Invalidate()
                    Case Else
                        Return _Value
                        Invalidate()
                End Select
            End Get
            Set(V As Integer)
                Select Case V
                    Case Is > _Maximum
                        V = _Maximum
                        Invalidate()
                End Select
                _Value = V
                Invalidate()
            End Set
        End Property
#End Region
#Region " Events "
        Protected Overrides Sub OnResize(e As EventArgs)
            MyBase.OnResize(e)
            Height = 25
        End Sub

        Protected Overrides Sub CreateHandle()
            MyBase.CreateHandle()
            Height = 25
        End Sub

        Public Sub Increment(ByVal Amount As Integer)
            Value += Amount
        End Sub
#End Region
#Region " Paint "
        Sub New()
            SetStyle(ControlStyles.AllPaintingInWmPaint Or ControlStyles.UserPaint Or _
                     ControlStyles.ResizeRedraw Or ControlStyles.OptimizedDoubleBuffer, True)
            DoubleBuffered = True
        End Sub

        Protected Overrides Sub OnPaint(e As PaintEventArgs)
            MyBase.OnPaint(e)
            Dim G = e.Graphics

            G.TextRenderingHint = TextRenderingHint.ClearTypeGridFit
            G.SmoothingMode = SmoothingMode.HighQuality
            G.PixelOffsetMode = PixelOffsetMode.HighQuality

            G.Clear(Color.White)

            Dim ProgVal As Integer = CInt(_Value / _Maximum * Width)

            G.FillRectangle(New SolidBrush(DarkColor), New Rectangle(0, 0, ProgVal - 1, Height))
            G.FillRectangle(New SolidBrush(LightColor), New Rectangle(0, 0, ProgVal - 1, Height / 2))

            G.FillRectangle(New SolidBrush(Parent.BackColor), New Rectangle(0, 0, 1, 1))
            G.FillRectangle(New SolidBrush(Parent.BackColor), New Rectangle(0, 1, 1, 1))
            G.FillRectangle(New SolidBrush(Parent.BackColor), New Rectangle(1, 0, 1, 1))
            G.FillRectangle(New SolidBrush(Parent.BackColor), New Rectangle(Width - 1, 0, 1, 1))
            G.FillRectangle(New SolidBrush(Parent.BackColor), New Rectangle(Width - 1, 1, 1, 1))
            G.FillRectangle(New SolidBrush(Parent.BackColor), New Rectangle(Width - 2, 0, 1, 1))
            G.FillRectangle(New SolidBrush(Parent.BackColor), New Rectangle(0, Height - 1, 1, 1))
            G.FillRectangle(New SolidBrush(Parent.BackColor), New Rectangle(0, Height - 2, 1, 1))
            G.FillRectangle(New SolidBrush(Parent.BackColor), New Rectangle(1, Height - 1, 1, 1))
            G.FillRectangle(New SolidBrush(Parent.BackColor), New Rectangle(Width - 1, Height - 1, 1, 1))
            G.FillRectangle(New SolidBrush(Parent.BackColor), New Rectangle(Width - 1, Height - 2, 1, 1))
            G.FillRectangle(New SolidBrush(Parent.BackColor), New Rectangle(Width - 2, Height - 1, 1, 1))

            G.InterpolationMode = CType(7, InterpolationMode)
        End Sub
#End Region
    End Class
    Public Class DarkThemeTabControl : Inherits TabControl
#Region " Paint "
        Sub New()
            MyBase.New()
            SetStyle(ControlStyles.AllPaintingInWmPaint Or _
            ControlStyles.ResizeRedraw Or _
            ControlStyles.UserPaint Or _
            ControlStyles.DoubleBuffer, True)
        End Sub
        Protected Overrides Sub OnPaint(ByVal e As PaintEventArgs)
            MyBase.OnPaint(e)
            Dim G = e.Graphics
            G.Clear(Color2)
            G.DrawRectangle(New Pen(New SolidBrush(Parent.BackColor)), New Rectangle(0, 24, Width - 1, Height - 25))
            G.DrawRectangle(New Pen(New SolidBrush(Parent.BackColor)), New Rectangle(0, 0, Width - 1, 24))
            Dim ItemBounds As Rectangle
            Using TextBrush As New SolidBrush(Color3)
                Using TabBrush As New SolidBrush(LightColor)
                    'Draw the tab items
                    For TabItemIndex As Integer = 0 To Me.TabCount - 1
                        ItemBounds = Me.GetTabRect(TabItemIndex)
                        Dim BorderPen As Pen
                        If Me.SelectedIndex = TabItemIndex Then
                            TabBrush.Color = Color2
                            BorderPen = New Pen(Color3, 1)
                        Else
                            TabBrush.Color = DarkColor
                            BorderPen = New Pen(LightColor, 1)
                        End If
                        G.FillRectangle(TabBrush, ItemBounds)
                        G.DrawRectangle(BorderPen, New Rectangle(ItemBounds.Location, New Size(ItemBounds.Width - 2, ItemBounds.Height - 1)))
                        BorderPen.Dispose()
                        Dim sf As New StringFormat
                        sf.LineAlignment = StringAlignment.Center
                        sf.Alignment = StringAlignment.Center
                        G.DrawString(Me.TabPages(TabItemIndex).Text, Me.Font, TextBrush, RectangleF.op_Implicit(Me.GetTabRect(TabItemIndex)), sf)
                        Try
                            Me.TabPages(TabItemIndex).BackColor = Color2
                        Catch
                        End Try
                    Next
                End Using
            End Using
            Try
                For Each tabpg As TabPage In Me.TabPages
                    tabpg.BorderStyle = BorderStyle.None
                Next
            Catch
            End Try
        End Sub
#End Region
    End Class
    Public Class DarkThemeDivider : Inherits Control
#Region " Properties "
        Protected Overrides Sub OnResize(e As EventArgs)
            MyBase.OnResize(e)
            Height = 2
        End Sub
#End Region
#Region " Paint "
        Protected Overrides Sub OnPaint(e As PaintEventArgs)
            MyBase.OnPaint(e)
            Dim g As Graphics = e.Graphics
            g.FillRectangle(New SolidBrush(Color.FromArgb(45, 45, 45)), New Rectangle(0, 0, Width, Height / 2))
            g.FillRectangle(New SolidBrush(Color.FromArgb(35, 35, 35)), New Rectangle(0, Height / 2, Width, Height / 2))
        End Sub
#End Region
    End Class
    Public Class DarkThemeComboBox : Inherits ComboBox
#Region " Properties "
        Sub New()
            MyBase.New()
            SetStyle(ControlStyles.AllPaintingInWmPaint Or _
            ControlStyles.ResizeRedraw Or _
            ControlStyles.UserPaint Or _
            ControlStyles.DoubleBuffer, True)
            DrawMode = Windows.Forms.DrawMode.OwnerDrawFixed
            BackColor = Color.Black
            ForeColor = Color.White
            DropDownStyle = ComboBoxStyle.DropDownList
        End Sub
        Private _StartIndex As Integer = 0
        Public Property StartIndex As Integer
            Get
                Return _StartIndex
            End Get
            Set(ByVal value As Integer)
                _StartIndex = value
                Try
                    MyBase.SelectedIndex = value
                Catch
                End Try
                Invalidate()
            End Set
        End Property
#End Region
#Region " Paint "
        Protected Overrides Sub OnPaint(ByVal e As PaintEventArgs)
            MyBase.OnPaint(e)
            Dim G As Graphics = e.Graphics
            Try
                With G
                    G.SmoothingMode = SmoothingMode.HighQuality
                    .Clear(Color3)
                    .FillRectangle(New SolidBrush(Color1), New Rectangle(Width - 22, 1, 20, Height - 2))
                    .DrawRectangle(New Pen(New SolidBrush(LightColor)), New Rectangle(0, 0, Width - 1, Height - 1))
                    .DrawRectangle(New Pen(New SolidBrush(LightColor)), New Rectangle(Width - 21, 1, 19, Height - 3))
                    .DrawLine(New Pen(New SolidBrush(DarkColor)), Width - 22, 0, Width - 22, Height)
                    DrawTriangle(Color3, New Point(Width - 15, 8), New Point(Width - 7, 8), New Point(Width - 11, 16), G)
                    .DrawString(Items(SelectedIndex).ToString, Font, New SolidBrush(Color2), New Point(3, 3))
                End With
            Catch
            End Try
        End Sub
        Sub ReplaceItem(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DrawItemEventArgs) Handles Me.DrawItem
            e.DrawBackground()
            Try
                If (e.State And DrawItemState.Selected) = DrawItemState.Selected Then
                    e.Graphics.FillRectangle(New SolidBrush(Color1), e.Bounds)
                End If
                Using b As New SolidBrush(e.ForeColor)
                    e.Graphics.DrawString(MyBase.GetItemText(MyBase.Items(e.Index)), e.Font, b, e.Bounds)
                End Using
            Catch
            End Try
            e.DrawFocusRectangle()
        End Sub
        Protected Sub DrawTriangle(ByVal Clr As Color, ByVal FirstPoint As Point, ByVal SecondPoint As Point, ByVal ThirdPoint As Point, ByVal G As Graphics)
            Dim points As New List(Of Point)()
            points.Add(FirstPoint)
            points.Add(SecondPoint)
            points.Add(ThirdPoint)
            G.FillPolygon(New SolidBrush(Clr), points.ToArray)
        End Sub
#End Region
    End Class
    Public Class DarkThemeDataGridView : Inherits DataGridView
#Region " Paint "
        Sub New()
            MyBase.New()
            SetStyle(ControlStyles.AllPaintingInWmPaint Or _
            ControlStyles.ResizeRedraw Or _
            ControlStyles.UserPaint Or _
            ControlStyles.DoubleBuffer, True)
            BackgroundColor = Color2
        End Sub
        Protected Sub CellPaint(ByVal sender As Object, ByVal e As DataGridViewCellPaintingEventArgs) Handles MyBase.CellPainting
            Dim G As Graphics = e.Graphics
            Try
                If e.RowIndex = -1 Then
                    G.FillRectangle(New SolidBrush(Color1), e.CellBounds)
                    G.DrawRectangle(New Pen(New SolidBrush(LightColor)), New Rectangle(e.CellBounds.X, e.CellBounds.Y, e.CellBounds.Width - 1, e.CellBounds.Height - 1))
                    With MyBase.ColumnHeadersDefaultCellStyle
                        G.DrawString(e.Value.ToString, .Font, New SolidBrush(Color3), e.CellBounds)
                    End With
                Else
                    G.FillRectangle(New SolidBrush(Color3), e.CellBounds)
                    G.DrawRectangle(New Pen(New SolidBrush(LightColor)), New Rectangle(e.CellBounds.X, e.CellBounds.Y, e.CellBounds.Width - 1, e.CellBounds.Height - 1))
                    With MyBase.ColumnHeadersDefaultCellStyle
                        Try
                            G.DrawImage(e.Value, New Point(e.CellBounds.X + e.CellBounds.Width / 2 - e.Value.Size.Width / 2, e.CellBounds.Y + e.CellBounds.Height / 2 - e.Value.Size.Height / 2))
                        Catch
                            G.DrawString(e.Value.ToString, .Font, New SolidBrush(Color2), e.CellBounds)
                        End Try
                    End With
                End If
            Catch
            End Try
            Try
                Dim current As DataGridViewCell = MyBase.Item(e.ColumnIndex, e.RowIndex)
                If current.Selected Then
                    G.FillRectangle(New SolidBrush(Color2), e.CellBounds)
                    With MyBase.ColumnHeadersDefaultCellStyle
                        Try
                            G.DrawImage(e.Value, New Point(e.CellBounds.X + e.CellBounds.Width / 2 - e.Value.Size.Width / 2, e.CellBounds.Y + e.CellBounds.Height / 2 - e.Value.Size.Height / 2))
                        Catch
                            G.DrawString(e.Value.ToString, .Font, New SolidBrush(Color3), e.CellBounds)
                        End Try
                    End With
                End If
            Catch
            End Try
            e.Handled = True
        End Sub
        Protected Overrides Sub OnPaint(ByVal e As PaintEventArgs)
            MyBase.OnPaint(e)
            BackgroundColor = Color2
            Dim G As Graphics = e.Graphics
            Try
                With G
                    .DrawRectangle(New Pen(New SolidBrush(LightColor), 1), New Rectangle(0, 0, Width - 1, Height - 1))
                End With
            Catch
            End Try
        End Sub
#End Region
    End Class
    Public Class DarkThemeGroupBox : Inherits ContainerControl
#Region " Paint "
        Sub New()
            BackColor = Color2
        End Sub
        Protected Overrides Sub OnPaint(e As PaintEventArgs)
            MyBase.OnPaint(e)
            Dim g As Graphics = e.Graphics
            Dim f As New Font("Segoe UI", 9, FontStyle.Bold)
            g.Clear(Parent.BackColor)
            Dim removeSize As SizeF = g.MeasureString(Text, f)
            g.FillPath(New SolidBrush(Color2), RoundRec(New Rectangle(0, 10, Width - 1, Height - 11), 4))
            g.DrawPath(New Pen(New SolidBrush(LightColor)), RoundRec(New Rectangle(0, 10, Width - 1, Height - 11), 4))
            g.DrawLine(New Pen(New SolidBrush(Color2)), 4, 10, removeSize.Width + 5, 10)
            g.DrawString(Text, f, New SolidBrush(Color3), New Point(5, 0))
        End Sub
#End Region
    End Class
    Public Class DarkThemeLabel : Inherits Label
#Region " Paint "
        Sub New()
            Dim f As New Font("Segoe UI", 9, FontStyle.Bold)
            Font = f
            ForeColor = Color3
        End Sub
        Protected Overrides Sub OnPaint(e As PaintEventArgs)
            MyBase.OnPaint(e)
            BackColor = Parent.BackColor
        End Sub
#End Region
    End Class
End Namespace