﻿
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.DarkTheme1 = New DarkGreenTheme.Controls.DarkTheme()
        Me.DarkThemeTabControl1 = New DarkGreenTheme.Controls.DarkThemeTabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.DarkThemeGroupBox1 = New DarkGreenTheme.Controls.DarkThemeGroupBox()
        Me.DarkThemeToggle2 = New DarkGreenTheme.Controls.DarkThemeToggle()
        Me.DarkThemeToggle1 = New DarkGreenTheme.Controls.DarkThemeToggle()
        Me.DarkThemeTextBox1 = New DarkGreenTheme.Controls.DarkThemeTextBox()
        Me.DarkThemeRadioButton2 = New DarkGreenTheme.Controls.DarkThemeRadioButton()
        Me.DarkThemeRadioButton1 = New DarkGreenTheme.Controls.DarkThemeRadioButton()
        Me.DarkThemeProgressBar1 = New DarkGreenTheme.Controls.DarkThemeProgressBar()
        Me.DarkThemeButton1 = New DarkGreenTheme.Controls.DarkThemeButton()
        Me.DarkThemeCheckBox2 = New DarkGreenTheme.Controls.DarkThemeCheckBox()
        Me.DarkThemeDataGridView1 = New DarkGreenTheme.Controls.DarkThemeDataGridView()
        Me.test = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.test2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.test3 = New System.Windows.Forms.DataGridViewImageColumn()
        Me.DarkThemeCheckBox1 = New DarkGreenTheme.Controls.DarkThemeCheckBox()
        Me.DarkThemeDivider1 = New DarkGreenTheme.Controls.DarkThemeDivider()
        Me.DarkThemeComboBox1 = New DarkGreenTheme.Controls.DarkThemeComboBox()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.DarkThemeControlBox1 = New DarkGreenTheme.Controls.DarkThemeControlBox()
        Me.DarkTheme1.SuspendLayout()
        Me.DarkThemeTabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.DarkThemeGroupBox1.SuspendLayout()
        CType(Me.DarkThemeDataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DarkTheme1
        '
        Me.DarkTheme1.BackColor = System.Drawing.Color.FromArgb(CType(CType(25, Byte), Integer), CType(CType(25, Byte), Integer), CType(CType(25, Byte), Integer))
        Me.DarkTheme1.Controls.Add(Me.DarkThemeTabControl1)
        Me.DarkTheme1.Controls.Add(Me.DarkThemeControlBox1)
        Me.DarkTheme1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DarkTheme1.Location = New System.Drawing.Point(0, 0)
        Me.DarkTheme1.Name = "DarkTheme1"
        Me.DarkTheme1.Size = New System.Drawing.Size(531, 443)
        Me.DarkTheme1.TabIndex = 0
        Me.DarkTheme1.Text = "DarkTheme1"
        '
        'DarkThemeTabControl1
        '
        Me.DarkThemeTabControl1.Controls.Add(Me.TabPage1)
        Me.DarkThemeTabControl1.Controls.Add(Me.TabPage2)
        Me.DarkThemeTabControl1.Location = New System.Drawing.Point(12, 36)
        Me.DarkThemeTabControl1.Name = "DarkThemeTabControl1"
        Me.DarkThemeTabControl1.SelectedIndex = 0
        Me.DarkThemeTabControl1.Size = New System.Drawing.Size(507, 395)
        Me.DarkThemeTabControl1.TabIndex = 1
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.Black
        Me.TabPage1.Controls.Add(Me.DarkThemeGroupBox1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 25)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(499, 366)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "TabPage1"
        '
        'DarkThemeGroupBox1
        '
        Me.DarkThemeGroupBox1.BackColor = System.Drawing.Color.Black
        Me.DarkThemeGroupBox1.Controls.Add(Me.DarkThemeToggle2)
        Me.DarkThemeGroupBox1.Controls.Add(Me.DarkThemeToggle1)
        Me.DarkThemeGroupBox1.Controls.Add(Me.DarkThemeTextBox1)
        Me.DarkThemeGroupBox1.Controls.Add(Me.DarkThemeRadioButton2)
        Me.DarkThemeGroupBox1.Controls.Add(Me.DarkThemeRadioButton1)
        Me.DarkThemeGroupBox1.Controls.Add(Me.DarkThemeProgressBar1)
        Me.DarkThemeGroupBox1.Controls.Add(Me.DarkThemeButton1)
        Me.DarkThemeGroupBox1.Controls.Add(Me.DarkThemeCheckBox2)
        Me.DarkThemeGroupBox1.Controls.Add(Me.DarkThemeDataGridView1)
        Me.DarkThemeGroupBox1.Controls.Add(Me.DarkThemeCheckBox1)
        Me.DarkThemeGroupBox1.Controls.Add(Me.DarkThemeDivider1)
        Me.DarkThemeGroupBox1.Controls.Add(Me.DarkThemeComboBox1)
        Me.DarkThemeGroupBox1.Location = New System.Drawing.Point(6, 6)
        Me.DarkThemeGroupBox1.Name = "DarkThemeGroupBox1"
        Me.DarkThemeGroupBox1.Size = New System.Drawing.Size(487, 354)
        Me.DarkThemeGroupBox1.TabIndex = 6
        Me.DarkThemeGroupBox1.Text = "DarkThemeGroupBox1"
        '
        'DarkThemeToggle2
        '
        Me.DarkThemeToggle2.BackColor = System.Drawing.Color.Black
        Me.DarkThemeToggle2.Checked = True
        Me.DarkThemeToggle2.Location = New System.Drawing.Point(207, 68)
        Me.DarkThemeToggle2.Name = "DarkThemeToggle2"
        Me.DarkThemeToggle2.Size = New System.Drawing.Size(80, 25)
        Me.DarkThemeToggle2.TabIndex = 11
        Me.DarkThemeToggle2.Text = "DarkThemeToggle2"
        '
        'DarkThemeToggle1
        '
        Me.DarkThemeToggle1.BackColor = System.Drawing.Color.Black
        Me.DarkThemeToggle1.Checked = False
        Me.DarkThemeToggle1.Location = New System.Drawing.Point(207, 99)
        Me.DarkThemeToggle1.Name = "DarkThemeToggle1"
        Me.DarkThemeToggle1.Size = New System.Drawing.Size(80, 25)
        Me.DarkThemeToggle1.TabIndex = 10
        Me.DarkThemeToggle1.Text = "DarkThemeToggle1"
        '
        'DarkThemeTextBox1
        '
        Me.DarkThemeTextBox1.BackColor = System.Drawing.Color.Transparent
        Me.DarkThemeTextBox1.Location = New System.Drawing.Point(28, 31)
        Me.DarkThemeTextBox1.MaxLength = 32767
        Me.DarkThemeTextBox1.Multiline = False
        Me.DarkThemeTextBox1.Name = "DarkThemeTextBox1"
        Me.DarkThemeTextBox1.ReadOnly = False
        Me.DarkThemeTextBox1.Size = New System.Drawing.Size(152, 29)
        Me.DarkThemeTextBox1.TabIndex = 9
        Me.DarkThemeTextBox1.Text = "DarkThemeTextBox1"
        Me.DarkThemeTextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.DarkThemeTextBox1.UseSystemPasswordChar = False
        '
        'DarkThemeRadioButton2
        '
        Me.DarkThemeRadioButton2.Checked = True
        Me.DarkThemeRadioButton2.Location = New System.Drawing.Point(298, 85)
        Me.DarkThemeRadioButton2.Name = "DarkThemeRadioButton2"
        Me.DarkThemeRadioButton2.Size = New System.Drawing.Size(186, 16)
        Me.DarkThemeRadioButton2.TabIndex = 8
        Me.DarkThemeRadioButton2.Text = "DarkThemeRadioButton2"
        '
        'DarkThemeRadioButton1
        '
        Me.DarkThemeRadioButton1.Checked = False
        Me.DarkThemeRadioButton1.Location = New System.Drawing.Point(298, 63)
        Me.DarkThemeRadioButton1.Name = "DarkThemeRadioButton1"
        Me.DarkThemeRadioButton1.Size = New System.Drawing.Size(186, 16)
        Me.DarkThemeRadioButton1.TabIndex = 7
        Me.DarkThemeRadioButton1.Text = "DarkThemeRadioButton1"
        '
        'DarkThemeProgressBar1
        '
        Me.DarkThemeProgressBar1.Location = New System.Drawing.Point(28, 138)
        Me.DarkThemeProgressBar1.Maximum = 100
        Me.DarkThemeProgressBar1.Name = "DarkThemeProgressBar1"
        Me.DarkThemeProgressBar1.Size = New System.Drawing.Size(259, 25)
        Me.DarkThemeProgressBar1.TabIndex = 6
        Me.DarkThemeProgressBar1.Text = "DarkThemeProgressBar1"
        Me.DarkThemeProgressBar1.Value = 50
        '
        'DarkThemeButton1
        '
        Me.DarkThemeButton1.Location = New System.Drawing.Point(28, 66)
        Me.DarkThemeButton1.Name = "DarkThemeButton1"
        Me.DarkThemeButton1.Size = New System.Drawing.Size(152, 31)
        Me.DarkThemeButton1.TabIndex = 0
        Me.DarkThemeButton1.Text = "DarkThemeButton1"
        '
        'DarkThemeCheckBox2
        '
        Me.DarkThemeCheckBox2.Checked = True
        Me.DarkThemeCheckBox2.Location = New System.Drawing.Point(298, 41)
        Me.DarkThemeCheckBox2.Name = "DarkThemeCheckBox2"
        Me.DarkThemeCheckBox2.Size = New System.Drawing.Size(166, 16)
        Me.DarkThemeCheckBox2.TabIndex = 2
        Me.DarkThemeCheckBox2.Text = "DarkThemeCheckBox2"
        '
        'DarkThemeDataGridView1
        '
        Me.DarkThemeDataGridView1.BackgroundColor = System.Drawing.Color.Black
        Me.DarkThemeDataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DarkThemeDataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.test, Me.test2, Me.test3})
        Me.DarkThemeDataGridView1.Location = New System.Drawing.Point(28, 180)
        Me.DarkThemeDataGridView1.Name = "DarkThemeDataGridView1"
        Me.DarkThemeDataGridView1.Size = New System.Drawing.Size(396, 153)
        Me.DarkThemeDataGridView1.TabIndex = 4
        '
        'test
        '
        Me.test.HeaderText = "Column1"
        Me.test.Name = "test"
        '
        'test2
        '
        Me.test2.HeaderText = "Column1"
        Me.test2.Name = "test2"
        '
        'test3
        '
        Me.test3.HeaderText = "Column1"
        Me.test3.Name = "test3"
        '
        'DarkThemeCheckBox1
        '
        Me.DarkThemeCheckBox1.Checked = False
        Me.DarkThemeCheckBox1.Location = New System.Drawing.Point(298, 19)
        Me.DarkThemeCheckBox1.Name = "DarkThemeCheckBox1"
        Me.DarkThemeCheckBox1.Size = New System.Drawing.Size(166, 16)
        Me.DarkThemeCheckBox1.TabIndex = 1
        Me.DarkThemeCheckBox1.Text = "DarkThemeCheckBox1"
        '
        'DarkThemeDivider1
        '
        Me.DarkThemeDivider1.Location = New System.Drawing.Point(3, 130)
        Me.DarkThemeDivider1.Name = "DarkThemeDivider1"
        Me.DarkThemeDivider1.Size = New System.Drawing.Size(481, 2)
        Me.DarkThemeDivider1.TabIndex = 5
        Me.DarkThemeDivider1.Text = "DarkThemeDivider1"
        '
        'DarkThemeComboBox1
        '
        Me.DarkThemeComboBox1.BackColor = System.Drawing.Color.Black
        Me.DarkThemeComboBox1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.DarkThemeComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.DarkThemeComboBox1.ForeColor = System.Drawing.Color.White
        Me.DarkThemeComboBox1.FormattingEnabled = True
        Me.DarkThemeComboBox1.Location = New System.Drawing.Point(293, 138)
        Me.DarkThemeComboBox1.Name = "DarkThemeComboBox1"
        Me.DarkThemeComboBox1.Size = New System.Drawing.Size(152, 21)
        Me.DarkThemeComboBox1.StartIndex = 0
        Me.DarkThemeComboBox1.TabIndex = 3
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.Black
        Me.TabPage2.Location = New System.Drawing.Point(4, 25)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(499, 366)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "TabPage2"
        '
        'DarkThemeControlBox1
        '
        Me.DarkThemeControlBox1.Location = New System.Drawing.Point(471, 0)
        Me.DarkThemeControlBox1.Name = "DarkThemeControlBox1"
        Me.DarkThemeControlBox1.Size = New System.Drawing.Size(60, 30)
        Me.DarkThemeControlBox1.TabIndex = 0
        Me.DarkThemeControlBox1.Text = "DarkThemeControlBox1"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(531, 443)
        Me.Controls.Add(Me.DarkTheme1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.TransparencyKey = System.Drawing.Color.Fuchsia
        Me.DarkTheme1.ResumeLayout(False)
        Me.DarkThemeTabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.DarkThemeGroupBox1.ResumeLayout(False)
        CType(Me.DarkThemeDataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DarkTheme1 As DarkGreenTheme.Controls.DarkTheme
    Friend WithEvents DarkThemeTabControl1 As DarkGreenTheme.Controls.DarkThemeTabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents DarkThemeDivider1 As DarkGreenTheme.Controls.DarkThemeDivider
    Friend WithEvents DarkThemeDataGridView1 As DarkGreenTheme.Controls.DarkThemeDataGridView
    Friend WithEvents test As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents test2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents test3 As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents DarkThemeComboBox1 As DarkGreenTheme.Controls.DarkThemeComboBox
    Friend WithEvents DarkThemeCheckBox2 As DarkGreenTheme.Controls.DarkThemeCheckBox
    Friend WithEvents DarkThemeCheckBox1 As DarkGreenTheme.Controls.DarkThemeCheckBox
    Friend WithEvents DarkThemeButton1 As DarkGreenTheme.Controls.DarkThemeButton
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents DarkThemeControlBox1 As DarkGreenTheme.Controls.DarkThemeControlBox
    Friend WithEvents DarkThemeGroupBox1 As DarkGreenTheme.Controls.DarkThemeGroupBox
    Friend WithEvents DarkThemeToggle2 As DarkGreenTheme.Controls.DarkThemeToggle
    Friend WithEvents DarkThemeToggle1 As DarkGreenTheme.Controls.DarkThemeToggle
    Friend WithEvents DarkThemeTextBox1 As DarkGreenTheme.Controls.DarkThemeTextBox
    Friend WithEvents DarkThemeRadioButton2 As DarkGreenTheme.Controls.DarkThemeRadioButton
    Friend WithEvents DarkThemeRadioButton1 As DarkGreenTheme.Controls.DarkThemeRadioButton
    Friend WithEvents DarkThemeProgressBar1 As DarkGreenTheme.Controls.DarkThemeProgressBar

End Class
